import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:my_box2d/my_box2d.dart';

void main() {
  const MethodChannel channel = MethodChannel('my_box2d');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await MyBox2d.platformVersion, '42');
  });
}
